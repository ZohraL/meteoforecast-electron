export const updateForecast = (forecast) => {
    return {
        type: 'UPDATE_FORECAST',
        value: forecast
    }
};

export const toggleLoader = (status) => {
    return {
        type: 'TOGGLE_LOADER',
        value: status
    }
};

export const fetchForecast = (city) => {
    return async dispatch => {
        dispatch(toggleLoader(true));
        const response = await fetch('http://api.weatherstack.com/current?access_key=705ba5c759d02997e5eb642194311f54&query='+city);
        const data = await response.json();
        console.log(data);
        dispatch(updateForecast(data));
        dispatch(toggleLoader(false));
    }
};