import React from 'react';
import Forecast from './Forecast';

function App() {
  return (
    <div>
      
        <Forecast />
    </div>
  );
}

export default App;
